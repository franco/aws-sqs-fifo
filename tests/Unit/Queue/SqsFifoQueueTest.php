<?php

declare(strict_types=1);

namespace WellTreasurePh\AwsSqsFifo\Tests\Unit\Queue;

use Aws\Result;
use Aws\Sqs\SqsClient;
use Mockery\MockInterface;
use WellTreasurePh\AwsSqsFifo\Tests\AbstractTestCase;
use WellTreasurePh\AwsSqsFifo\Queue\SqsFifoQueue;

/**
 * @covers \WellTreasurePh\AwsSqsFifo\Queue\SqsFifoQueue 
 */
final class SqsFifoQueueTest extends AbstractTestCase 
{
    public function testPushRawShouldOverrideSqsQueuePayload(): void
    {
        /** @var \Aws\Sqs\SqsClient $sqs */
        $sqs = $this->mock(SqsClient::class, function (MockInterface $mock): void {
            $result = $this->mock(Result::class, static function (MockInterface $mock): void {
                    $mock->shouldReceive('get')
                        ->once(1);
                });

            $mock->shouldReceive('sendMessage')
                ->once()
                ->withArgs(static function (...$args): bool {
                    $keys = \reset($args);

                    self::assertArrayHasKey('MessageGroupId', $keys);
                    self::assertArrayHasKey('MessageDeduplicationId', $keys);

                    return true;
                })->andReturn($result);
        });

        $fifo = new SqsFifoQueue($sqs, 'test', 'test', 'test', 'test');
        $fifo->pushRaw((string)\json_encode(['sample' => 'payload']));
    }
}
