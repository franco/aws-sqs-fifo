<?php

declare(strict_types=1);

namespace WellTreasurePh\AwsSqsFifo\Tests\Unit;

use Illuminate\Queue\QueueManager;
use Illuminate\Queue\QueueServiceProvider;
use Laravel\Lumen\Application;
use WellTreasurePh\AwsSqsFifo\Connector\SqsFifoConnector;
use WellTreasurePh\AwsSqsFifo\SqsFifoServiceProvider;
use WellTreasurePh\AwsSqsFifo\Tests\AbstractTestCase;

/**
 * @covers \WellTreasurePh\AwsSqsFifo\SqsFifoServiceProvider
 */
final class SqsFifoServiceProviderTest extends AbstractTestCase
{
    public function testRegister(): void
    {
        $app = new Application(__DIR__);
        $app->register(QueueServiceProvider::class);
        $app->register(SqsFifoServiceProvider::class);

        $connectors = $this->getPropertyAsPublic(QueueManager::class, 'connectors');

        $closure = $connectors->getValue($app->get('queue'))['sqsfifo'];

        self::assertInstanceOf(SqsFifoConnector::class, $closure());
    }
}
