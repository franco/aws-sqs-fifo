<?php

declare(strict_types=1);

namespace WellTreasurePh\AwsSqsFifo\Tests\Unit\Connector;

use WellTreasurePh\AwsSqsFifo\Tests\AbstractTestCase;
use WellTreasurePh\AwsSqsFifo\Connector\SqsFifoConnector;
use WellTreasurePh\AwsSqsFifo\Queue\SqsFifoQueue;

/**
 * @covers \WellTreasurePh\AwsSqsFifo\Connector\SqsFifoConnector
 */
final class SqsFifoConnectorTest extends AbstractTestCase 
{
    public function testConnectShouldReturnSqsFifoQueue(): void
    {
        $connector = new SqsFifoConnector();

        self::assertInstanceOf(SqsFifoQueue::class, $connector->connect([
            'queue' => 'sample',
            'prefix' => 'sample',
            'region' => 'sample-region',
        ]));
    }
}
