<?php

declare(strict_types=1);

namespace WellTreasurePh\AwsSqsFifo\Tests;

use Closure;
use Mockery;
use Mockery\MockInterface;
use PHPUnit\Framework\TestCase;
use ReflectionProperty;

abstract class AbstractTestCase extends TestCase
{
    public function mock(string $class, ?Closure $mock = null): MockInterface
    {
        $args[] = $class;

        if ($mock !== null) {
            $args[] = $mock;
        }

        /** @var \Mockery\MockInterface $mockery */
        $mockery = \call_user_func_array([new Mockery, 'mock'], $args);

        return $mockery;
    }

    protected function getPropertyAsPublic(string $class, string $property): ReflectionProperty
    {
        $reflection = new ReflectionProperty($class, $property);
        $reflection->setAccessible(true);

        return $reflection;
    }
}
