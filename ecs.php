<?php

declare(strict_types=1);

use SlevomatCodingStandard\Sniffs\Namespaces\ReferenceUsedNamesOnlySniff;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symplify\CodingStandard\Fixer\ArrayNotation\ArrayOpenerAndCloserNewlineFixer;
use Symplify\CodingStandard\Fixer\LineLength\LineLengthFixer;
use Symplify\EasyCodingStandard\ValueObject\Option;

return static function (ContainerConfigurator $containerConfigurator): void {
    $parameters = $containerConfigurator->parameters();

    $parameters->set(Option::SETS, ['symplify']);

    $parameters->set(Option::PATHS, [__DIR__ . '/src', __DIR__ . '/tests']);

    // Skip rules
    $parameters->set(Option::SKIP, [
        ReferenceUsedNamesOnlySniff::class . '.ReferenceViaFullyQualifiedName' => null,
        LineLengthFixer::class => null,
        ArrayOpenerAndCloserNewlineFixer::class => null,
    ]);
};
