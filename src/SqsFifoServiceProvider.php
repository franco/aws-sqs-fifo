<?php

declare(strict_types=1);

namespace WellTreasurePh\AwsSqsFifo;

use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Queue\QueueManager;
use Illuminate\Support\ServiceProvider;
use WellTreasurePh\AwsSqsFifo\Connector\SqsFifoConnector;

final class SqsFifoServiceProvider extends ServiceProvider implements DeferrableProvider
{
    public function register(): void
    {
        $this->app->afterResolving('queue', function (QueueManager $manager): void {
            $manager->addConnector(
                'sqsfifo',
                function (): SqsFifoConnector {
                    return new SqsFifoConnector;
                }
            );
        });
    }

    /**
     * @return string[] 
     */
    public function provides(): array
    {
        return [
            'queue',
        ];
    }
}
