<?php

declare(strict_types=1);

namespace WellTreasurePh\AwsSqsFifo\Queue;

use Aws\Sqs\SqsClient;
use Illuminate\Queue\SqsQueue;

final class SqsFifoQueue extends SqsQueue 
{
    private ?string $messageGroupId;

    public function __construct(
        SqsClient $sqs, 
        string $default, string $prefix = '', 
        string $suffix = '', 
        ?string $messageGroupId = null
    ) {
        $this->messageGroupId = $messageGroupId ?? \uniqid();

        parent::__construct($sqs, $default, $prefix, $suffix);
    }

    /**
     * Push a raw payload onto the queue.
     *
     * @param string $payload
     * @param string|null $queue
     * @param mixed[] $options
     * 
     * @return mixed
     */
    public function pushRaw($payload, $queue = null, array $options = [])
    {
        return $this->sqs->sendMessage([
            'QueueUrl' => $this->getQueue($queue), 
            'MessageBody' => $payload,
            'MessageGroupId' => $this->messageGroupId,
            'MessageDeduplicationId' => \uniqid(),
        ])->get('MessageId');
    }
}
