<?php

declare(strict_types=1);

namespace WellTreasurePh\AwsSqsFifo\Connector;

use Aws\Sqs\SqsClient;
use Illuminate\Queue\Connectors\ConnectorInterface;
use Illuminate\Support\Arr;
use Illuminate\Contracts\Queue\Queue;
use WellTreasurePh\AwsSqsFifo\Queue\SqsFifoQueue;

final class SqsFifoConnector implements ConnectorInterface
{
    /**
     * Establish a queue connection.
     *
     * @param mixed[] $config
     */
    public function connect(array $config): Queue
    {
        $config = $this->getDefaultConfiguration($config);

        if (! empty($config['key']) && ! empty($config['secret'])) {
            $config['credentials'] = Arr::only($config, ['key', 'secret', 'token']);
        }

        return new SqsFifoQueue(
            new SqsClient($config), 
            $config['queue'], 
            $config['prefix'] ?? '', 
            $config['suffix'] ?? '',
            $config['message_group_id'] ?? null
        );
    }

    /**
     * Get the default configuration for SQS.
     *
     * @param  mixed[]  $config
     * 
     * @return mixed[]
     */
    protected function getDefaultConfiguration(array $config): array
    {
        return \array_merge([
            'version' => 'latest',
            'http' => [
                'timeout' => 60,
                'connect_timeout' => 60,
            ],
        ], $config);
    }
}